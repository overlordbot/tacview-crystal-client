require "option_parser"

require "./client"
require "./base_processor"

options = {
  "host"        => "",
  "password"    => nil,
  "port"        => 42_674,
  "client_name" => "crystal_tacview_client",
}

OptionParser.parse do |parser|
  parser.banner = "Usage: ./tacview_client [options]"

  parser.on("-o", "--host=host", "Tacview server hostname / IP " \
                                 "(Required)") do |v|
    options["host"] = v
  end

  parser.on("-p", "--port=port", "Tacview server port (Default: 42674)") do |v|
    options["port"] = v.to_i
  end

  parser.on("-a", "--password=password", "Tacview server password " \
                                         "(Optional)") do |v|
    options["password"] = v
  end

  parser.on("-c", "--client-name=client", "Client name (Default: " \
                                          "crystal_tacview_client)") do |v|
    options["client_name"] = v
  end

  parser.on("-h", "--help", "Show this help") do
    puts parser
    exit
  end
end

if options["host"].as(String).empty?
  puts "Hostname required"
  exit(1)
end

if options["client_name"].as(String).empty?
  puts "client-name cannot be empty"
  exit(1)
end

# A very simple processor that outputs all events received from a
# TacviewClient::Reader instance to the console. Useful for simple
# testing apps
class ConsoleOutputter < TacviewClient::BaseProcessor
  def update_object(object)
    puts "Object Update  : #{object}"
  end

  def delete_object(object_id)
    puts "Object Deletion: #{object_id}"
  end

  def update_time(time)
    puts "Time Update    : #{time}"
  end

  def set_property(property, value)
    puts "Property Set   : #{property}, #{value}"
  end
end

client = TacviewClient::Client.new(
  processor: ConsoleOutputter.new,
  host: options["host"].as(String),
  port: options["port"].as(Int32),
  password: options["password"],
  client_name: options["client_name"].as(String)
)
client.connect
