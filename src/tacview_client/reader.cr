require "big"
require "./reader/parser"
require "./base_processor"

module TacviewClient
  # Reads events from an input source, parses them using the {Parser}
  # and calls the appropriate event processor method
  class Reader
    # The format that matches the beginning of an ACMI update line
    OBJECT_UPDATE_MARKER = /^[0-9a-f]{2,},/i

    # The format that matches the beginning of an ACMI delete line
    OBJECT_DELETION_MARKER = '-'

    # The format that matches the beginning of an ACMI time update line
    TIME_UPDATE_MARKER = '#'

    # The format that matches the beginning of an ACMI Property line
    GLOBAL_PROPERTY_MARKER = "0,"

    # @param input_source [IO, #gets] An {IO} object (or object that implements
    #   the {IO#gets} method. Typically this is a Socket or File.
    # @param processor [BaseProcessor] The object that processes the events
    #   emitted by the {Reader}.
    def initialize(@input_source : IO, @processor : BaseProcessor)
    end

    def start_reading : Bool
      while (line = @input_source.gets)
        route_line(line)
      end
      true
    ensure
      @input_source.close
    end

    private def route_line(line : String) : Nil
      if line =~ OBJECT_UPDATE_MARKER
        object_update(line)
      elsif line[0] == TIME_UPDATE_MARKER
        time_update(line)
      elsif line[0] == OBJECT_DELETION_MARKER
        object_deletion(line)
      elsif line[0..1] == GLOBAL_PROPERTY_MARKER
        global_property(line)
      end
    end

    private def object_update(line)
      result = Parser.new(line).parse_object_update
      @processor.update_object(result) if result
    end

    private def object_deletion(line)
      @processor.delete_object line[1..-1]
    end

    private def time_update(line)
      @processor.update_time BigDecimal.new(line[1..-1])
    end

    private def global_property(line)
      key, value = line[2..-1].split("=")

      if value.ends_with?("\\")
        value = [value] + read_multiline
        value = value.reduce([] of String) do |arr, array_line|
          arr << array_line.delete("\\").strip
        end.join("\n")
      end

      @processor.set_property(property: key, value: value.strip)
    end

    private def read_multiline
      array_lines = [] of String
      while (line = @input_source.gets)
        array_lines << line
        break unless line.ends_with?("\\")
      end

      array_lines
    end
  end
end
