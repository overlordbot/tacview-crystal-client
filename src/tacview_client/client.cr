require "socket"

require "./base_processor"
require "./reader"

module TacviewClient
  # The actual client to be instantiated to connect to a Tacview Server
  class Client
    # The underlying stream protocol used by Tacview. Needs to be in-sync
    # between the client and the server.
    STREAM_PROTOCOL = "XtraLib.Stream.0"

    # The application level protocol used by Tacview. Needs to be in-sync
    # between the client and the server.
    TACVIEW_PROTOCOL = "Tacview.RealTimeTelemetry.0"

    # A null terminator used by Tacview to terminate handshake packages
    HANDSHAKE_TERMINATOR = "\0"

    # Returns a new instance of a Client
    #
    # This is the entry point into the gem. Instantiate an instance of this
    # class to setup the prerequisite data for a connection to a Tacview server.
    # Once done call {#connect} to start processing the Tacview ACMI stream.
    #
    # @param host [String] Server hostname or IP
    # @param port [Integer] Server port
    # @param password [String] Plaintext password required to connect to a
    #   password protected Tacview server. Is hashed before transmission.
    # @param client_name [String] Client name to send to the server
    # @param processor [BaseProcessor] The object that processes the events
    #   emitted by the {Reader}.
    def initialize(@processor : BaseProcessor,
                   @host : String,
                   @port : Int32 = 42_674,
                   @password : Int32 | String | Nil = nil,
                   @client_name : String = "crystal_tacview_client")
      @connection = TCPSocket.new
    end

    # Connect to the Tacview server
    #
    # Actually opens a TCP connection to the Tacview server and starts
    # streaming ACMI lines to an instance of the {Reader} class.
    #
    # This method will only return when the TCP connection has be killed
    # either by a client-side signal or by the server closing the TCP
    # connection.
    def connect
      @connection.connect(@host, @port)
      raise "Could not connect" unless @connection

      read_handshake

      send_handshake

      start_reader
    end

    # See https://www.tacview.net/documentation/realtime/en/
    # for information on connection negotiation
    private def read_handshake
      stream_protocol = read_handshake_header :stream_protocol
      validate_handshake_header STREAM_PROTOCOL, stream_protocol

      tacview_protocol = read_handshake_header :tacview_protocol
      validate_handshake_header TACVIEW_PROTOCOL, tacview_protocol

      read_handshake_header :host

      @connection.gets HANDSHAKE_TERMINATOR
    end

    # Header parameter included for logging purposes
    private def read_handshake_header(_header)
      @connection.gets
    end

    private def validate_handshake_header(expected, actual)
      return if expected == actual

      abort_connection
    end

    private def abort_connection
      @connection.close
      exit(1)
    end

    private def send_handshake
      @connection.print [
        STREAM_PROTOCOL,
        TACVIEW_PROTOCOL,
        @client_name,
        @password,
      ].join("\n") + HANDSHAKE_TERMINATOR
    end

    # Currently not called, instead we just use the password as provided by the
    # caller of the client and make them hash it instead.
    private def hash_password
      return 0 unless @password
      NotImplementedError.new("No support for CRC-64-ECMA in Crystal or any" \
                              "shards yet")
    end

    private def start_reader
      reader = Reader.new(input_source: @connection,
        processor: @processor)
      reader.start_reading
    end
  end
end
