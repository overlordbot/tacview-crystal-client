require "big"

module TacviewClient
  # An Base Procesor that defines all the methods called by an instance of
  # a TacviewClient::Reader. Your processor must inherit from this class
  # and implement each method.
  abstract class BaseProcessor
    # Process an update event for an object
    #
    # On the first appearance of the object there are usually more fields
    # including pilot names, object type etc.
    #
    # For existing objects these events are almost always lat/lon/alt updates
    # only
    #
    # @param event [Hash] A parsed ACMI line. This hash will always include
    #   the fields listed below but may also include others depending on the
    #   source line.
    # @option event [String] :object_id The hexadecimal format object ID.
    # @option event [BigDecimal] :latitude The object latitude in WGS 84 format.
    # @option event [BigDecimal] :longitude The object latitude in WGS 84
    #   format.
    # @option event [BigDecimal] :altitude The object altitude above sea level
    #   in meters to 1 decimal place.
    abstract def update_object(event : Hash(String, String | BigDecimal))

    # Process a delete event for an object
    #
    # @param object_id [String] A hexadecimal number representing the object
    #   ID
    abstract def delete_object(object_id : String)

    # Process a time update event
    #
    # @param time [BigDecimal] A time update in seconds from the
    #   ReferenceTime to 2 decimal places
    abstract def update_time(time : BigDecimal)

    # Set a property
    #
    # @param property [String] The name of the property to be set
    # @param value [String] The value of the property to be set
    abstract def set_property(property : String, value : String)
  end
end
