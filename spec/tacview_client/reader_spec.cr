require "../spec_helper"
require "../../src/tacview_client/reader"

class NullProcessor < TacviewClient::BaseProcessor
  def update_object(object); end

  def delete_object(object_id); end

  def update_time(time); end

  def set_property(property, value); end
end

Spectator.describe TacviewClient::Reader do
  context "when finished reading an input source" do
    let(simple_io) { IO::Memory.new("a\n") }
    let(null_processor) { NullProcessor.new }
    subject { described_class.new input_source: simple_io,
      processor: null_processor }

    it "returns true" do
      expect(subject.start_reading).to be_true
    end

    it "closes the IO object" do
      subject.start_reading
      expect(simple_io).to be_closed
    end
  end
end
