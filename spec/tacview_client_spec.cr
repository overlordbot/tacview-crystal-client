require "./spec_helper"
require "../src/tacview_client"

Spectator.describe TacviewClient do
  it "requires the Client" do
    expect(TacviewClient::Client).to be_truthy
  end

  it "requires the BaseProcessor" do
    expect(TacviewClient::BaseProcessor).to be_truthy
  end

  it "requires the Reader" do
    expect(TacviewClient::Reader).to be_truthy
  end
end
